import * as _ from 'lodash';
import { Component, OnInit, Input, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { _ParseAST } from '@angular/compiler';
import { SponsorService } from 'src/app/services/api/sponsor.service';
import { BoothService } from 'src/app/services/api/booth.service';

declare var $: any;

@Component({
  selector: 'app-sponsor',
  templateUrl: './sponsor.component.html',
  styleUrls: ['./sponsor.component.scss'],
})

export class SponsorComponent implements OnInit {
  @Input('sponsor') sponsor: any;
  @Output('detailFn') detailFn = new EventEmitter();

  constructor(
    private boothService: BoothService,
  ) { }
  public attachments: any[];
  public tourSuccess = false;

  ngOnInit(): void {
    this.attachments = this.setAttachments();
    $('.modal').on('hidden.bs.modal', () => {
      this.removeDesc('sponsorModal_' + this.sponsor.id);
    });
  }

  setAttachments(): any[] {
    if (this.sponsor.contents) {
      return _.map(this.sponsor.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  setDesc(id): void {
    const user = JSON.parse(localStorage.getItem('cfair'));

    const sponsorId = id.split('_')[1];
    this.boothService.findOne(sponsorId).subscribe(res => {
      this.sponsor = res;

      if (this.sponsor.description) {
        document.getElementById(id + 'desc').innerHTML = this.sponsor.description;
      }
    });
  }

  /**
   * 모달 닫힐 때 해당 스폰서의 description을 제거한다.
   * description에 동영상이 재생 중이면 꺼지지 않기때문에 비워줄 필요 있다.
   */
  removeDesc(id): void {
    if (document.getElementById(id + 'desc').innerHTML) {
      document.getElementById(id + 'desc').innerHTML = null;
    }
  }
}

