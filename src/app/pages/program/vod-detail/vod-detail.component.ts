import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { VodService } from 'src/app/services/api/vod.service';
import { NgImageSliderComponent } from 'ng-image-slider';
import { BannerService } from 'src/app/services/api/banner.service';
import { MemberService } from 'src/app/services/api/member.service';
import { HistoryService } from 'src/app/services/api/history.service';

@Component({
  selector: 'app-vod-detail',
  templateUrl: './vod-detail.component.html',
  styleUrls: ['./vod-detail.component.scss']
})
export class VodDetailComponent implements OnInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;

  public vod: any;
  public vodId: string;

  private user: any;
  public next: any;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    public vodService: VodService,
    private bannerService: BannerService,
    private memberService: MemberService,
    private historyService: HistoryService,
  ) { }

  ngOnInit(): void {
    this.historyService.setAttendance('in');
    this.user = JSON.parse(localStorage.getItem('cfair'));

    this.activatedRoute.params.subscribe(params => {
      if (params.vodId) {
        this.vodService.findOne(params.vodId).subscribe(vod => {
          this.vod = vod.contents;
          this.vodId = vod.id;
        });
        // this.getBanners();
      }
    });
  }

  // 리스트로 돌아가기
  goBack(): void {
    this.location.back();
  }

}
