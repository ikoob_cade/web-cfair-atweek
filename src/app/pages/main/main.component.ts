import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { SpeakerService } from 'src/app/services/api/speaker.service';
import { BoothService } from 'src/app/services/api/booth.service';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef
    ;
  public speakers: Array<any> = [];
  public sponsors: Array<any> = []; // 스폰서 목록
  public selectedBooth: any;
  public mobile: boolean;

  public programBookUrl = '';
  public excerptUrl = '';

  public user;

  public attachments = []; // 부스 첨부파일

  constructor(
    private speakerService: SpeakerService,
    private boothService: BoothService,
    private cookieService: CookieService,
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    if (window.innerWidth < 768 !== this.mobile) {
      this.loadSpeakers();
    }
  }

  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)]
      })
    );
  }

  ngOnInit(): void {
    this.user = localStorage.getItem('cfair');
    this.loadSpeakers();
    this.loadBooths();
  }

  ngAfterViewInit(): void {
    if (this.cookieService.get(`${environment.eventId}_main_popup`)) {
      let cookieTime = this.cookieService.get(`${environment.eventId}_main_popup`);
      const now = new Date().getTime().toString();
      if (cookieTime < now) {
        this.cookieService.delete(`${environment.eventId}_main_popup`);
        this.policyAlertBtn.nativeElement.click();
      }
    } else {
      this.policyAlertBtn.nativeElement.click();
    }
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers(): void {
    let limit = 6;
    this.mobile = window.innerWidth < 768;
    if (this.mobile) { limit = 6; }
    this.speakerService.find(false).subscribe(res => {
      this.speakers = this.division(res, limit);
    });
  }

  /**
   * 스폰서 목록 조회
   */
  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      this.sponsors = res;
    });
  }

  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  // 부스 상세보기
  getDetail = (selectedBooth) => {
    this.boothService.findOne(selectedBooth.id).subscribe(res => {
      this.selectedBooth = res;
      this.setAttachments();
      this.setDesc();
    });
  }

}
